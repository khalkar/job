angular.module('userApp', [ "ngResource" ]).

config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/login', {
		templateUrl : 'html/login.html',
		controller : LoginCtrl
	}).when('/register', {
		templateUrl : 'html/register.html',
		controller : RegCtrl
	}).otherwise({
		redirectTo : '/login'
	});
} ]);